O programa recebe como entrada um arquivo texto com uma lista de vértices na primeira linha e uma lista de arestas na segunda (ver arquivo de exemplo grafo.txt). O programa é capaz de identificar um valor para a aresta (terceiro atributo), quando informado. Caso não seja informado, é atribuído o valor 1.

O programa também encontra o maior Clique do grafo utilizando duas abordagens: força bruta e o algoritmo guloso Bron–Kerbosch.

# Compilar
```
javac src/app/*.java -d bin
```

# Executar
```
java -cp bin app.App grafo.txt
```