package app;

import java.util.HashSet;
import java.util.Set;

/**
 * Clique
 */
public class Clique {

    private Set<Vertice> vertices = new HashSet<Vertice>();

    public Clique(Set<Vertice> vertices) {
        this.vertices = vertices;
    }

    public Set<Vertice> getVertices() {
        return vertices;
    }

    public void setVertices(Set<Vertice> vertices) {
        this.vertices = vertices;
    }

    public void addVertice(Vertice v) {
        this.vertices.add(v);
    }

    public int tamanho() {
        return this.vertices.size();
    }

    @Override
    public String toString() {
        return "Clique [vertices=" + vertices + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((vertices == null) ? 0 : vertices.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Clique other = (Clique) obj;
        if (vertices == null) {
            if (other.vertices != null)
                return false;
        } else if (!vertices.equals(other.vertices))
            return false;
        return true;
    }

}