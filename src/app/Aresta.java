package app;

/**
 * Aresta
 * 
 * A classe armazena o valor (peso) da aresta,
 * além do outro vértice que completa a ligação.
 */
public class Aresta {

    private int peso;
    private Vertice verticeFinal;

    public Aresta(int peso, Vertice verticeFinal) {
        this.peso = peso;
        this.verticeFinal = verticeFinal;
    }

    public Aresta(Vertice verticeFinal) {
        this.peso = 1;
        this.verticeFinal = verticeFinal;
    }

    public int getPeso() {
        return peso;
    }

    public Vertice getVerticeFinal() {
        return verticeFinal;
    }

    @Override
    public String toString() {
        return " -(" + getPeso() + ")-> " + getVerticeFinal().getValor();
    }

    @Override
    public boolean equals(Object obj) {
        Aresta aresta = (Aresta) obj;
        return aresta.getVerticeFinal() == verticeFinal;
    }

}