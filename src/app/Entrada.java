package app;

/**
 * Entrada
 * 
 * Estrutura que armazena os vértices e arestas
 * lidos a partir do arquivo texto de entrada.
 */
public class Entrada {

    private int[] vertices;
    private String[] arestas;

    public int[] getVertices() {
        return vertices;
    }

    public void setVertices(int[] vertices) {
        this.vertices = vertices;
    }

    public String[] getArestas() {
        return arestas;
    }

    public void setArestas(String[] arestas) {
        this.arestas = arestas;
    }

}