package app;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

/**
 * App
 */
public class App {

    // Processa arquivo de entrada
    private static Entrada lerEntrada(String caminhoArquivo) {

        Entrada entrada = new Entrada();

        try {
            FileInputStream arquivo = new FileInputStream(caminhoArquivo);
            InputStreamReader isr = new InputStreamReader(arquivo);
            LineNumberReader reader = new LineNumberReader(isr);
            String linha = "";
            while ((linha = reader.readLine()) != null && reader.getLineNumber() <= 2) {
                String[] itens = linha.split(",(?![^(]*\\))");
                if (reader.getLineNumber() == 1) {
                    int[] vertices = new int[itens.length];
                    for (int i = 0; i < itens.length; i++) {
                        vertices[i] = Integer.parseInt(itens[i]);
                    }
                    entrada.setVertices(vertices);
                } else if (reader.getLineNumber() == 2) {
                    entrada.setArestas(itens);
                }
            }
			reader.close();
        } catch (FileNotFoundException e) {
            System.err.println("Arquivo não encontrado.");
        } catch (IOException e) {
            System.err.println("Não foi possível ler o arquivo.");
        }

        return entrada;

    }

    // Método principal
    public static void main(String[] args) {
        
        Entrada e = App.lerEntrada(args[0]);
        Grafo g = new Grafo();

        for (int vertice : e.getVertices()) {
            g.addVertice(vertice);
        }

        for (String aresta : e.getArestas()) {
            aresta = aresta.replaceAll("[()]", "");
            String[] arestaStrings = aresta.split(",");
            int valorVerticeInicio = Integer.parseInt(arestaStrings[0]);
            int valorVerticeFim = Integer.parseInt(arestaStrings[1]);
            int valorAresta = 0;
            if (arestaStrings.length > 2) {
                valorAresta = Integer.parseInt(arestaStrings[2]);
            }
            Vertice verticeInicio = g.getVertice(valorVerticeInicio);
            Vertice verticeFim = g.getVertice(valorVerticeFim);
            verticeInicio.addAresta(valorAresta, verticeFim);
        }

        System.out.println("Lista de adjacência: ");
        g.imprime();

        System.out.print("Caminhamento em profundidade: ");
        g.buscaDFS(g.getVertice(0));

        System.out.print("\nOrdenação topológica: ");
        g.ordenacaoTopologica();
        System.out.println();

        System.out.println("\nComponentes fortemente conexos:");
        g.componentesFortementeConexos();  

        System.out.println("\nMaior Clique (Força bruta):");
        g.maxCliqueForcaBruta();

        System.out.println("\nMaior Clique (Abordagem gulosa):");
        g.maxCliqueGuloso();
        
    }

}