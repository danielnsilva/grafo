package app;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Vertice
 * 
 * Representação do vértice.
 * Cada vértice possui um valor e uma lista de arestas.
 */
public class Vertice {

    private int valor;
    private ArrayList<Aresta> arestas = new ArrayList<Aresta>();

    public Vertice(int valor) {
        this.valor = valor;
    }
    
    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public ArrayList<Aresta> getArestas() {
        return arestas;
    }

    public void addAresta(int valor, Vertice v) {
        Aresta a = new Aresta(valor, v);
        this.arestas.add(a);
    }

    public void addAresta(Aresta a) {
        this.arestas.add(a);
    }

    public Set<Vertice> getVerticesAdj() {
        Set<Vertice> vertices = new HashSet<Vertice>();
        for (Aresta a : this.arestas) {
            vertices.add(a.getVerticeFinal());
        }
        return vertices;
    }

    @Override
    public String toString() {
        return Integer.toString(getValor());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + valor;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Vertice other = (Vertice) obj;
        if (valor != other.valor)
            return false;
        return true;
    }

    

}