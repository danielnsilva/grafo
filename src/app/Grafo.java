package app;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.Stack;

/**
 * Grafo
 * 
 * Classe que representa um grafo e armazena seus vértices.
 */
public class Grafo {

    // Lista de vértices
    private ArrayList<Vertice> vertices = new ArrayList<Vertice>();

    // Retorna lista de vértices do grafo
    public ArrayList<Vertice> getVertices() {
        return vertices;
    }

    // Adiciona um vértice ao grafo com o valor passado
    public void addVertice(int valor) {
        Vertice v = new Vertice(valor);
        this.vertices.add(v);
    }

    // Adciona um vértice (objeto Vertice) ao grafo
    public void addVertice(Vertice v) {
        this.vertices.add(v);
    }

    // Retorna um vértice
    public Vertice getVertice(int valor) {
        Vertice vertice = null;
        for (Vertice v : this.vertices) {
            if (valor == v.getValor()) {
                vertice = v;
                break;
            }
        }
        return vertice;
    }

    // Retorna uma string com a lista de adjacência do grafo
    public String listaAdjacencia() {
        StringBuilder listaAdjacencias = new StringBuilder();
        for (Vertice v : this.vertices) {
            listaAdjacencias.append(v);
            for (Aresta a : v.getArestas()) {
                listaAdjacencias.append(a);
            }
            listaAdjacencias.append("\n");
        }
        return listaAdjacencias.toString();
    }

    // Outro método para exibir o conteúdo do objeto
    public void imprime() {
        System.out.println(this);
    }

    // Realiza busca em largura
    public void buscaBFS(Vertice v) {
        ArrayList<Vertice> visitados = new ArrayList<Vertice>();
        LinkedList<Vertice> fila = new LinkedList<Vertice>();
        visitados.add(v);
        fila.add(v);
        while (fila.size() > 0) {
            v = fila.poll();
            System.out.print(v + " ");
            for (Aresta a : v.getArestas()) {
                Vertice vFinal = a.getVerticeFinal();
                if (!visitados.contains(vFinal)) {
                    visitados.add(vFinal);
                    fila.add(vFinal);
                }
            }
        }
        System.out.println();
    }

    // Realiza busca em largura (método principal)
    // Inicia no primeiro vértice da lista
    public void buscaDFS() {
        ArrayList<Vertice> visitados = new ArrayList<Vertice>();
        Vertice v = this.vertices.get(0);
        visitaDFS(v, visitados);
        System.out.println();
    }

    // Realiza busca em profundidade (método principal)
    // Inicia no vértice indicado
    public void buscaDFS(Vertice v) {
        ArrayList<Vertice> visitados = new ArrayList<Vertice>();
        visitaDFS(v, visitados);
        System.out.println();
    }

    // Realiza busca em largura (método auxiliar)
    private void visitaDFS(Vertice v, ArrayList<Vertice> visitados) {
        visitados.add(v);
        System.out.print(v + " ");
        for (Aresta a : v.getArestas()) {
            Vertice vFinal = a.getVerticeFinal();
            if (!visitados.contains(vFinal)) {
                visitaDFS(vFinal, visitados);
            }
        }
    }

    // Lista componentes conexos, baseado na busca em largura. 
    public void componentesConexos() {
        ArrayList<Vertice> visitados = new ArrayList<Vertice>();
        for (Vertice v : vertices) {
            if (!visitados.contains(v)) {
                visitaDFS(v, visitados);
                System.out.println();
            }
        }
    }

    // Cria o graof transposto
    private Grafo grafoTransposto() {
        Grafo g = new Grafo();
        for (Vertice v : this.vertices) {
            g.addVertice(v.getValor());
        }
        for (Vertice v : this.vertices) {
            for (Aresta a : v.getArestas()) {
                int valor = a.getVerticeFinal().getValor();
                Vertice verticeInicio = g.getVertice(valor);
                Vertice verticeFim = g.getVertice(v.getValor());
                verticeInicio.addAresta(a.getPeso(), verticeFim);
            }
        }        
        return g;
    }

    // Lista componentes fortemente conexos,
    // baseado na busca em largura a partir do grafo transposto.
    public void componentesFortementeConexos() {
        Grafo g = this.grafoTransposto();
        g.componentesConexos();
    }

    // Ordenação topológica (método auxiliar)
    private void visitaTopologica(Vertice v, ArrayList<Vertice> visitados, Stack<Vertice> vertices) {
		visitados.add(v);
        for (Aresta a : v.getArestas()) {
            Vertice vFinal = a.getVerticeFinal();
            if (!visitados.contains(vFinal)) {
                visitaTopologica(vFinal, visitados, vertices);
            }
        }
        vertices.add(v);
    }
    
    // Ordenação topológica (método principal)
    public void ordenacaoTopologica() {
        ArrayList<Vertice> visitados = new ArrayList<Vertice>();
        Stack<Vertice> vertices = new Stack<Vertice>();
        for (Vertice v : this.vertices) {
            if (!visitados.contains(v)) {
                visitaTopologica(v, visitados, vertices);                
            }
        }
        while (!vertices.isEmpty()) {
            Vertice v = vertices.pop();
            System.out.print(v.getValor() + " ");
        }
    }

    /**
     * Problema do clique utilizando força bruta
     */
    public void maxCliqueForcaBruta() {

        long inicio = System.currentTimeMillis();
        ArrayList<Clique> cliques = identificaCliques();
        long fim = System.currentTimeMillis();
        Clique maior = new Clique(new HashSet<Vertice>());
        
        for (Clique c : cliques) {
            if (c.getVertices().size() >= maior.getVertices().size()) {
                maior = c;
            }
        }
        
        for (Clique c : cliques) {
            if (c.tamanho() == maior.tamanho()) {
                System.out.println(c);
            }
        }
        
        System.out.println("Tempo decorrido: " + (fim - inicio) + " ms.");

    }

    private ArrayList<Clique> identificaCliques() {

        ArrayList<Clique> cliques = new ArrayList<Clique>();
        ArrayList<Vertice> vertices = this.vertices;
        
        for (int i = 1, max = 1 << vertices.size(); i < max; ++i) {

            Clique clique = new Clique(new HashSet<Vertice>());

            for (int j = 0, k = 1; j < vertices.size(); ++j, k <<= 1) {
                if ((k & i) != 0) {
                    Vertice vertice = vertices.get(j);
                    boolean isClique = true;
                    for (Vertice v : clique.getVertices()) {
                        if (!v.getVerticesAdj().contains(vertice)) {
                            isClique = false;
                            break;
                        }
                    }
                    if (clique.tamanho() == 0 || isClique) {
                        clique.addVertice(vertice);
                    }
                }
            }

            if (clique.tamanho() > 1) {
                if (!cliques.contains(clique)) {
                    cliques.add(clique);
                }
            }

        }
        
        return cliques;

    }

    /**
     * Abordagem gulosa para o problema do clique
     * utilizando o algoritmo Bron–Kerbosch
     */
    public void maxCliqueGuloso() {

        Set<Vertice> R = new HashSet<Vertice>();
        Set<Vertice> P = new HashSet<Vertice>(this.vertices);
        Set<Vertice> X = new HashSet<Vertice>();
        ArrayList<Clique> cliques = new ArrayList<Clique>();
        Clique maior = new Clique(new HashSet<Vertice>());
        
        long inicio = System.currentTimeMillis();
        bronKerbosch(cliques, R, P, X);
        long fim = System.currentTimeMillis();

        for (Clique c : cliques) {
            if (c.getVertices().size() >= maior.getVertices().size()) {
                maior = c;
            }
        }

        for (Clique c : cliques) {
            if (c.tamanho() == maior.tamanho()) {
                System.out.println(c);
            }
        }

        System.out.println("Tempo decorrido: " + (fim - inicio) + " ms.");

    }

    // Implementação do algoritmo Bron–Kerbosch
    private void bronKerbosch(ArrayList<Clique> cliques, Set<Vertice> R, Set<Vertice> P, Set<Vertice> X) {
        
        if (P.isEmpty() && X.isEmpty()) {
            Clique c = new Clique(R);
            cliques.add(c);
        }

        Set<Vertice> P_temp = new HashSet<Vertice>(P);

        for (Vertice v : P_temp) {
            
            Set<Vertice> N = v.getVerticesAdj();

            Set<Vertice> R2 = new HashSet<Vertice>(R);
            Set<Vertice> P2 = new HashSet<Vertice>(P);
            Set<Vertice> X2 = new HashSet<Vertice>(X);

            R2.add(v);
            P2.retainAll(N);
            X2.retainAll(N);

            bronKerbosch(cliques, R2, P2, X2);

            P.remove(v);
            X.add(v);

        }

    }

    // Representação textual do objeto como uma lista de adjacência
    @Override
    public String toString() {
        return listaAdjacencia();
    }

}